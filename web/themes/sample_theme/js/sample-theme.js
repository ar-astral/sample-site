/**
 * @file
 * Sample Theme behaviors.
 */
(function (Drupal) {

  'use strict';

  Drupal.behaviors.sampleTheme = {
    attach: function (context, settings) {
      once('sampleTheme', 'body').forEach(function (body) {
        var x = document.createElement("BUTTON");
        var t = document.createTextNode(Drupal.t("Click me"));
        x.appendChild(t);
        x.onclick = function () {
          alert(settings.sampleModule.siteName);
        }
        body.appendChild(x);  
      });
    }
  };

} (Drupal));
