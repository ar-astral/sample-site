<?php

namespace Drupal\sample_module\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Sample Module settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sample_module_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sample_module.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['text_field'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text Field'),
      '#description' => $this->t('Text Field Description.'),
      '#default_value' => $this->config('sample_module.settings')->get('text_field.value'),
      '#format' => $this->config('sample_module.settings')->get('text_field.format')
    ];
    $form['integer_field'] = [
      '#type' => 'number',
      '#title' => $this->t('Integer Field'),
      '#description' => $this->t('Integer Field Description.'),
      '#default_value' => $this->config('sample_module.settings')->get('integer_field'),
      '#step' => 1
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Sample Empty field validation
    // if (empty($form_state->getValue('text_field'))) {
    //   $form_state->setErrorByName('text_field', $this->t('The text_field value is empty.'));
    // }

    if (filter_var($form_state->getValue('integer_field'), FILTER_VALIDATE_INT) === false) {
      $form_state->setErrorByName('integer_field', $this->t('The integer number require an integer value'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('sample_module.settings')
    ->set('text_field', $form_state->getValue('text_field'))
    ->set('integer_field', $form_state->getValue('integer_field'))
    ->save();
    parent::submitForm($form, $form_state);
  }

}
