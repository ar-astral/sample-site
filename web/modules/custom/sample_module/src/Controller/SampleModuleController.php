<?php

namespace Drupal\sample_module\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Sample Module routes.
 */
class SampleModuleController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $settings = $this->config('sample_module.settings');
    $build['content'] = [
      '#theme' => 'sample_module_settings',
      '#settings' => 
      [
        'text_field' => [
          'value' => $settings->get('text_field.value'),
          'format' => $settings->get('text_field.format')
        ],
        'integer_field' => $settings->get('integer_field')
      ]
    ];

    return $build;
  }

}
